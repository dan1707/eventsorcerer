package address

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/event"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/store"
)

type Service struct {
	store store.EventStore
}

func NewService(store store.EventStore) *Service {
	return &Service{store}
}

func (s *Service) CreateContact(name string) uuid.UUID {
	e := event.ContactCreated{Name: name}
	return s.store.Insert(e)
}

func (s *Service) ChangeContactName(uuid uuid.UUID, name string) error {
	e := event.ContactNameChanged{UUID: uuid, Name: name}
	return s.store.Update(uuid, e)
}

func (s *Service) FindContact(uuid uuid.UUID) (Contact, error) {
	var address Contact
	events, err := s.store.Find(uuid)
	if err != nil {
		return address, err
	}
	for _, ev := range events {
		switch e := ev.(type) {
		case event.ContactCreated:
			address = Contact{UUID: e.UUID, ContactName: e.Name}
		case event.ContactNameChanged:
			address.ContactName = e.Name
		}
	}
	return address, nil
}
