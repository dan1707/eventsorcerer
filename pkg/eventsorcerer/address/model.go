package address

import uuid "github.com/satori/go.uuid"

type Contact struct {
	UUID        uuid.UUID
	ContactName string
}
