package event

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
)

type Event interface {
	SetUUID(uuid uuid.UUID) Event
	String() string
}

type ContactCreated struct {
	UUID uuid.UUID
	Name string
}

func (e ContactCreated) SetUUID(uuid uuid.UUID) Event {
	e.UUID = uuid
	return e
}

func (e ContactCreated) String() string {
	return fmt.Sprintf("{ContactCreated UUID=%s ContactName=%s}", e.UUID, e.Name)
}

type ContactNameChanged struct {
	UUID uuid.UUID
	Name string
}

func (e ContactNameChanged) SetUUID(uuid uuid.UUID) Event {
	e.UUID = uuid
	return e
}

func (e ContactNameChanged) String() string {
	return fmt.Sprintf("{ContactNameChanged UUID=%s ContactName=%s}", e.UUID, e.Name)
}
