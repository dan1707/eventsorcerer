package store

import (
	"github.com/juju/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/event"
)

type MemEventLog []event.Event
type MemEventStore map[uuid.UUID]MemEventLog

type InMemoryStore struct {
	store MemEventStore
	log   MemEventLog
}

func Empty() *InMemoryStore {
	return &InMemoryStore{
		store: make(MemEventStore),
	}
}

func (s *InMemoryStore) Insert(e event.Event) uuid.UUID {
	id := uuid.NewV4()
	e = e.SetUUID(id)
	s.store[id] = MemEventLog{e}
	return id
}

func (s *InMemoryStore) Update(uuid uuid.UUID, event event.Event) error {
	if _, ok := s.store[uuid]; !ok {
		return errors.Errorf("Unknown ID %s", uuid)
	}
	s.store[uuid] = append(s.store[uuid], event)
	return nil
}

func (s *InMemoryStore) Find(uuid uuid.UUID) ([]event.Event, error) {
	if events, ok := s.store[uuid]; ok {
		return events, nil
	} else {
		return []event.Event{}, errors.Errorf("Unknown ID %s", uuid)
	}
}
