package store

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/event"
)

type EventStore interface {
	Insert(event event.Event) uuid.UUID
	Update(uuid uuid.UUID, event event.Event) error
	Find(uuid uuid.UUID) ([]event.Event, error)
}
