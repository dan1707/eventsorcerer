package main

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/address"
	"gitlab.com/dan1707/eventsorcerer/pkg/eventsorcerer/store"
	"math/rand"
	"reflect"
	"testing"
)

var FirstNames = []string{
	"Isaura",
	"Tawna",
	"Alma",
	"Crystal",
	"Weldon",
	"Henriette",
	"Daryl",
	"Detra",
	"Jeraldine",
	"Cameron",
	"Jamila",
	"Stephen",
	"Dede",
	"Latashia",
	"Sue",
	"Tijuana",
	"Tova",
	"Laurence",
	"Phebe",
	"Nydia",
}
var LastNames = []string{
	"Line",
	"Ortis",
	"Kohr",
	"Bourdon",
	"Mozingo",
	"Coldren",
	"Tony",
	"Yuen",
	"Mcmillen",
	"Rohan",
	"Bagwell",
	"Nawrocki",
	"Seager",
	"Cathcart",
	"Doman",
	"Ledger",
	"Ensign",
	"Tindel",
	"Samons",
	"Rueter",
}

func TestAddressBook(t *testing.T) {
	eventStore := store.Empty()
	addressBook := address.NewService(eventStore)

	numberOfContacts := 100
	numberOfNameChanges := 100
	contactIds := make([]uuid.UUID, numberOfContacts)

	// Create contacts
	for i := 0; i < numberOfContacts; i++ {
		contactName := fmt.Sprintf("%s %s",
			FirstNames[rand.Intn(len(FirstNames))], LastNames[rand.Intn(len(LastNames))])
		contactId := addressBook.CreateContact(contactName)
		contactIds[i] = contactId

		contact, err := addressBook.FindContact(contactId)
		if err != nil {
			t.Errorf("Expected contact for %s with contactId %s. Found nothing. %v", contactName, contactId, err)
		}
		if !reflect.DeepEqual(contact.UUID, contactId) {
			t.Errorf("Expected contactId: %s Got: %s", contactId, contact.UUID)
		}
		if contact.ContactName != contactName {
			t.Errorf("Expected contactName: %s Got: %s", contactName, contact.ContactName)
		}
	}

	// Change contact names
	for j := 0; j < numberOfNameChanges; j++ {
		for i := 0; i < numberOfContacts; i++ {
			contactName := fmt.Sprintf("%s %s",
				FirstNames[rand.Intn(len(FirstNames))], LastNames[rand.Intn(len(LastNames))])
			id := contactIds[i]
			if err := addressBook.ChangeContactName(id, contactName); err != nil {
				t.Fatalf("Changing contactName in address %s failed. %v", id, err)
			}
			contact, err := addressBook.FindContact(id)
			if err != nil {
				t.Errorf("Expected address for %s with id %s. Found nothing. %v", contactName, id, err)
			}
			if contact.ContactName != contactName {
				t.Errorf("Expected contactName: %s Got: %s", contactName, contact.ContactName)
			}
		}
	}
}
